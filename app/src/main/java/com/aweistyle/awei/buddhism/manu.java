package com.aweistyle.awei.buddhism;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class manu extends AppCompatActivity {

    String version;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manu);

         Button jingang = findViewById(R.id.jingang);
         final Button mahayana = findViewById(R.id.mahayana);
         Button name = findViewById(R.id.name);
         Button calculation = findViewById(R.id.calculation);
         Button exit = findViewById(R.id.exit);
         TextView title = findViewById(R.id.title_text);

        version = getVersionName(this);

        title.setText("dharma" + "_v" + version);

         jingang.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {

                 Intent intent = new Intent();
                 intent.setClass(manu.this,jingangSpell.class);
                 startActivity(intent);

             }
         });

         mahayana.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {

                 Intent intent = new Intent();
                 intent.setClass(manu.this,mahayana.class);
                 startActivity(intent);

             }
         });

         name.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {

                 Intent intent = new Intent();
                 intent.setClass(manu.this,name.class);
                 startActivity(intent);

             }
         });

        calculation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent();
                intent.setClass(manu.this,calculation.class);
                startActivity(intent);

            }
        });

        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });



    }

    public String getVersionName(Context context){

        PackageManager packageManager=context.getPackageManager();
        PackageInfo packageInfo;
        String versionName="";
        try {
            packageInfo=packageManager.getPackageInfo(context.getPackageName(),0);
            versionName=packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionName;
    }

}
