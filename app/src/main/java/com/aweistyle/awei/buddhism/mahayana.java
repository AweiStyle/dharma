package com.aweistyle.awei.buddhism;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.TimeZone;

public class mahayana extends AppCompatActivity {

    String mahayanaTag[] = {"選單","新增1","新增2","新增3","新增4","新增5","新增6","新增7","新增8","新增9","新增10"};
    Adapter_mahayana adapter_mahayana;
    String json;
    String mahayana_choose;
    int x;
    Spinner mahayana_spinner;
    TextView setTitle;
    ArrayList<String> arrayList;
    Gson gson;
    ArrayList<ItemClass> myList;
    String historyJ2;
    ArrayList timestampArr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mahayana);

        gson = new Gson();

        Button save = findViewById(R.id.save);
        Button cancel = findViewById(R.id.cancel);
        final Button modify = findViewById(R.id.modify);
        Button addItem = findViewById(R.id.addItem);
        setTitle = findViewById(R.id.setTitle);
        Button history = findViewById(R.id.history);

        mahayana_spinner = findViewById(R.id.mahayana_spinner);

        final TextView show_text = findViewById(R.id.show_text);

        json = getSharedPreferences("mahayanaTag_edit", MODE_PRIVATE).getString("mahayanaTag", "[\"選單\",\"新增1\",\"新增2\",\"新增3\",\"新增4\",\"新增5\",\"新增6\",\"新增7\",\"新增8\",\"新增9\",\"新增10\"]");

        Log.e("onCreate",json);
        mahayanaTag = gson.fromJson(json,String[].class);

        adapter_mahayana = new Adapter_mahayana(mahayanaTag);
        final Runnable r = new Runnable() {
            @Override
            public void run() {

                mahayana_spinner.setAdapter(adapter_mahayana);
                mahayana_spinner.setSelection(0);

            }
        };


        runOnUiThread(r);

        mahayana_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                arrayList = new ArrayList(Arrays.asList(mahayanaTag));
                String temp = arrayList.get(mahayana_spinner.getSelectedItemPosition());

                if(position == 0){

                    setTitle.setText("");
                    show_text.setText("");

                }else if (position == 1){

                    x = getSharedPreferences("a", MODE_PRIVATE).getInt("a", 0);
                    setTitle.setText(temp);
                    show_text.setText("總計：" + x + "部");

                }else if (position == 2){

                    x = getSharedPreferences("b", MODE_PRIVATE).getInt("a", 0);
                    setTitle.setText(temp);
                    show_text.setText("總計：" + x + "部");

                }else if (position == 3){

                    x = getSharedPreferences("c", MODE_PRIVATE).getInt("a", 0);
                    setTitle.setText(temp);
                    show_text.setText("總計：" + x + "部");

                }else if (position == 4){

                    x = getSharedPreferences("d", MODE_PRIVATE).getInt("a", 0);
                    setTitle.setText(temp);
                    show_text.setText("總計：" + x + "部");

                }else if (position == 5){

                    x = getSharedPreferences("e", MODE_PRIVATE).getInt("a", 0);
                    setTitle.setText(temp);
                    show_text.setText("總計：" + x + "部");

                }else if (position == 6){

                    x = getSharedPreferences("f", MODE_PRIVATE).getInt("a", 0);
                    setTitle.setText(temp);
                    show_text.setText("總計：" + x + "部");

                }else if (position == 7){

                    x = getSharedPreferences("g", MODE_PRIVATE).getInt("a", 0);
                    setTitle.setText(temp);
                    show_text.setText("總計：" + x + "部");

                }
                else if (position == 8){

                    x = getSharedPreferences("h", MODE_PRIVATE).getInt("a", 0);
                    setTitle.setText(temp);
                    show_text.setText("總計：" + x + "部");

                }else if (position == 9){

                    x = getSharedPreferences("i", MODE_PRIVATE).getInt("a", 0);
                    setTitle.setText(temp);
                    show_text.setText("總計：" + x + "部");

                }else if (position == 10){

                    x = getSharedPreferences("j", MODE_PRIVATE).getInt("a", 0);
                    setTitle.setText(temp);
                    show_text.setText("總計：" + x + "部");

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(mahayana.this);
                builder.setTitle("溫馨提醒");
                builder.setMessage("確定儲存次數嗎？");
                builder.setIcon(R.drawable.ic_launcher_background);
                builder.setPositiveButton("是", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        String mahayana = null;

                        try {

                            EditText input_ET = findViewById(R.id.input_ET);
                            String s = input_ET.getText().toString(); //輸入的次數
                            if (s.equals(""))throw new Error();
                            int i = Integer.parseInt(s);  //x = 總次數 , i = 次數的整數型態

                            if (i == 0){

                                Toast toast2 = Toast.makeText(mahayana.this, "追加數字不得為0", Toast.LENGTH_SHORT);
                                toast2.show();

                            }

                            arrayList = new ArrayList(Arrays.asList(mahayanaTag));
                            mahayana_choose = arrayList.get(mahayana_spinner.getSelectedItemPosition());

                            if(mahayana_spinner.getSelectedItemPosition() == 0){

                                show_text.setText("");

                                Toast toast2 = Toast.makeText(mahayana.this, "請選擇欲修改的經典", Toast.LENGTH_SHORT);
                                toast2.show();

                                return;

                            }

                            if (mahayana_spinner.getSelectedItemPosition() == 1) mahayana = "a";
                            if (mahayana_spinner.getSelectedItemPosition() == 2) mahayana = "b";
                            if (mahayana_spinner.getSelectedItemPosition() == 3) mahayana = "c";
                            if (mahayana_spinner.getSelectedItemPosition() == 4) mahayana = "d";
                            if (mahayana_spinner.getSelectedItemPosition() == 5) mahayana = "e";
                            if (mahayana_spinner.getSelectedItemPosition() == 6) mahayana = "f";
                            if (mahayana_spinner.getSelectedItemPosition() == 7) mahayana = "g";
                            if (mahayana_spinner.getSelectedItemPosition() == 8) mahayana = "h";
                            if (mahayana_spinner.getSelectedItemPosition() == 9) mahayana = "i";
                            if (mahayana_spinner.getSelectedItemPosition() == 10) mahayana = "j";

                            int y = getSharedPreferences(mahayana , MODE_PRIVATE).getInt("a",0);

                            x = y + i;

                            show_text.setText("總計：" + x + "部");
                            input_ET.setText("");

                            SharedPreferences pref = getSharedPreferences(mahayana, MODE_PRIVATE);
                            pref.edit()
                                    .putInt("a", x)
                                    .commit();

                            SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd_hh:mm");
                            format.setTimeZone(TimeZone.getDefault());

                            Date curDate = new Date(System.currentTimeMillis());

                            String str = format.format(curDate);

                            Gson gson = new Gson();

                            String timeStampStr = getSharedPreferences("timestamp", MODE_PRIVATE)
                                    .getString("timestamp2", "[]");

                            String historyJ = getSharedPreferences("historyJingangSpell", MODE_PRIVATE)
                                    .getString("historyArray2", "[]");

                            Log.e("historyJ",historyJ);
                            Log.e("timestamp",timeStampStr);

                            String str_i = String.valueOf(i);

                            timestampArr = gson.fromJson(timeStampStr,ArrayList.class);

                            myList = gson.fromJson(historyJ,ArrayList.class);

                            myList.add(new ItemClass(mahayana_spinner.getSelectedItem().toString(),"追加"+ "(" + str_i + ")",String.valueOf(x),str)); //( 名稱 / 內容 / 次數 / 時間 )
                            long timeStamp = System.currentTimeMillis();
                            timestampArr.add(String.valueOf(timeStamp));

                            timeStampStr = gson.toJson(timestampArr);



                            historyJ2 = gson.toJson(myList);

                            SharedPreferences pref1 = getSharedPreferences("historyJingangSpell", MODE_PRIVATE);
                            pref1.edit().putString("historyArray2",historyJ2).commit();

                            SharedPreferences pref2 = getSharedPreferences("timestamp", MODE_PRIVATE);
                            pref2.edit().putString("timestamp2",timeStampStr).commit();

                        }catch (Exception e){

                            Toast toast2 = Toast.makeText(mahayana.this, "請輸入合法數字", Toast.LENGTH_SHORT);
                            toast2.show();

                        }catch (Error error){

                            Toast toast2 = Toast.makeText(mahayana.this, "追加次數不得為空", Toast.LENGTH_SHORT);
                            toast2.show();

                        }

                    }

                });

                builder.setNegativeButton("否", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                    }
                });

                builder.show();

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });

        modify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final EditText edit1 = new EditText(mahayana.this);

                AlertDialog.Builder builder = new AlertDialog.Builder(mahayana.this);
                builder.setTitle("請輸入要修改的數字");    //设置对话框标题
                builder.setIcon(android.R.drawable.btn_star);

                edit1.setSingleLine();
                edit1.setImeOptions(EditorInfo.IME_ACTION_DONE);

                builder.setView(edit1);
                builder.setPositiveButton("確認", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                        try {

                            String mahayana = null;
                            String modify = edit1.getText().toString();

                            if (modify.equals(""))throw new Error();

                            Integer.parseInt(modify);

                            if (Integer.parseInt(modify)<0){

                                Toast toast2 = Toast.makeText(mahayana.this, "不得輸入負數", Toast.LENGTH_SHORT);
                                toast2.show();

                                return;

                            }

                            show_text.setText("總計：" + Integer.parseInt(modify) + "部");

                            ArrayList<String> arrayList = new ArrayList(Arrays.asList(mahayanaTag));
                            mahayana_choose = arrayList.get(mahayana_spinner.getSelectedItemPosition());

                            if (mahayana_spinner.getSelectedItemPosition() == 0){

                                show_text.setText("");

                                Toast toast2 = Toast.makeText(mahayana.this, "請選擇欲修改的經典", Toast.LENGTH_SHORT);
                                toast2.show();

                                return;

                            }

                            if (mahayana_spinner.getSelectedItemPosition() == 1) mahayana = "a";
                            if (mahayana_spinner.getSelectedItemPosition() == 2) mahayana = "b";
                            if (mahayana_spinner.getSelectedItemPosition() == 3) mahayana = "c";
                            if (mahayana_spinner.getSelectedItemPosition() == 4) mahayana = "d";
                            if (mahayana_spinner.getSelectedItemPosition() == 5) mahayana = "e";
                            if (mahayana_spinner.getSelectedItemPosition() == 6) mahayana = "f";
                            if (mahayana_spinner.getSelectedItemPosition() == 7) mahayana = "g";
                            if (mahayana_spinner.getSelectedItemPosition() == 8) mahayana = "h";
                            if (mahayana_spinner.getSelectedItemPosition() == 9) mahayana = "i";
                            if (mahayana_spinner.getSelectedItemPosition() == 10) mahayana = "j";

                            int temp = getSharedPreferences(mahayana , MODE_PRIVATE).getInt("a",0);

                            SharedPreferences pref = getSharedPreferences(mahayana, MODE_PRIVATE);
                            pref.edit()
                                    .putInt("a", Integer.valueOf(modify))
                                    .commit();

                            temp = Integer.valueOf(modify) - temp;
                            String m;  //判斷正負號
                            if(temp > 0){
                                m = "+";
                            }else {
                                m = "";
                            }




                            SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd_hh:mm");
                            format.setTimeZone(TimeZone.getDefault());

                            Date curDate = new Date(System.currentTimeMillis());

                            String str = format.format(curDate);

                            Gson gson = new Gson();

                            String timeStampStr = getSharedPreferences("timestamp", MODE_PRIVATE)
                                    .getString("timestamp2", "[]");

                            String historyJ = getSharedPreferences("historyJingangSpell", MODE_PRIVATE)
                                    .getString("historyArray2", "[]");

                            Log.e("historyJ",historyJ);
                            Log.e("timestamp",timeStampStr);

                            timestampArr = gson.fromJson(timeStampStr,ArrayList.class);

                            myList = gson.fromJson(historyJ,ArrayList.class);

                            myList.add(new ItemClass(mahayana_spinner.getSelectedItem().toString(),"修改"+ "(" + m + temp + ")",modify,str)); //( 名稱 / 內容 / 次數 / 時間 )
                            long timeStamp = System.currentTimeMillis();
                            timestampArr.add(String.valueOf(timeStamp));

                            timeStampStr = gson.toJson(timestampArr);

                            historyJ2 = gson.toJson(myList);

                            SharedPreferences pref1 = getSharedPreferences("historyJingangSpell", MODE_PRIVATE);
                            pref1.edit().putString("historyArray2",historyJ2).commit();

                            SharedPreferences pref2 = getSharedPreferences("timestamp", MODE_PRIVATE);
                            pref2.edit().putString("timestamp2",timeStampStr).commit();

                                Toast toast = Toast.makeText(mahayana.this, "修改內容已存檔", Toast.LENGTH_SHORT);
                                toast.show();


                        } catch (Exception e) {

                            Toast toast2 = Toast.makeText(mahayana.this, "請輸入合法數字", Toast.LENGTH_SHORT);
                            toast2.show();

                        }catch (Error error){

                            Toast toast2 = Toast.makeText(mahayana.this, "修改次數不得為空", Toast.LENGTH_SHORT);
                            toast2.show();

                        }


                    }
                });
                builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }


                });

                builder.show();

            }
        });

        history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(mahayana.this , SpellSearch.class);
                intent.putExtra("mark","maha");
                startActivity(intent);


            }
        });

        addItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                final EditText edit = new EditText(mahayana.this);

                AlertDialog.Builder builder = new AlertDialog.Builder(mahayana.this);
                builder.setTitle("請輸入要修改的名稱");    //设置对话框标题
                builder.setIcon(android.R.drawable.btn_star);

                edit.setSingleLine();
                edit.setImeOptions(EditorInfo.IME_ACTION_DONE);

                builder.setView(edit);
                builder.setPositiveButton("確認", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if(mahayana_spinner.getSelectedItemPosition() == 0){

                            Toast toast2 = Toast.makeText(mahayana.this, "請選擇欲修改的按鈕", Toast.LENGTH_SHORT);
                            toast2.show();

                            return;

                        }

                        try {

                            String editItem = edit.getText().toString();


                            if (editItem.equals(""))throw new Error();

                            if (mahayana_spinner.getSelectedItemPosition() == 0){

                                Toast toast = Toast.makeText(mahayana.this, "請選擇修改項目", Toast.LENGTH_SHORT);
                                toast.show();

                                return;

                            }

                            int position = mahayana_spinner.getSelectedItemPosition();
                            mahayanaTag[position] = editItem;

                            String json = gson.toJson(mahayanaTag);

                            Log.e("mahayanaTag_addItem",json);

                            SharedPreferences pref = getSharedPreferences("mahayanaTag_edit", MODE_PRIVATE);
                            pref.edit()
                                    .putString("mahayanaTag", json)
                                    .commit();

                            Toast toast = Toast.makeText(mahayana.this, "修改內容已存檔", Toast.LENGTH_SHORT);
                            toast.show();

                            runOnUiThread(r);



                        } catch (Exception e) {


                            Log.e(getClass().getName(), Log.getStackTraceString(e));

                            Log.e("click_catch","click_catch");


                        }catch (Error error){

                            Toast toast2 = Toast.makeText(mahayana.this, "修改項目不得為空", Toast.LENGTH_SHORT);
                            toast2.show();

                        }


                    }
                });
                builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }


                });

                builder.show();

            }
        });




    }
}
