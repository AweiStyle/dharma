package com.aweistyle.awei.buddhism;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class Adapter_dharma extends BaseAdapter {

    String[] dharmaTag;

    public Adapter_dharma (String dharmaTag[]){

        this.dharmaTag = dharmaTag;

    }

    @Override
    public int getCount() {
        return dharmaTag.length;
    }

    @Override
    public Object getItem(int position) {
        return dharmaTag[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.spinneritem_mahayana,null);

        TextView company = view.findViewById(R.id.getView);

        company.setText(dharmaTag[position]);

        return view;

    }
}
