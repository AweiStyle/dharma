package com.aweistyle.awei.buddhism;

import android.text.StaticLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.aweistyle.awei.buddhism.R;

import java.util.ArrayList;

public class ViewAdapterSearch extends BaseAdapter {

    ArrayList<ItemClass> myList;
    String mark;

    public ViewAdapterSearch(ArrayList<ItemClass> myList){

        this.myList = myList;

    }

    public ViewAdapterSearch(String mark){

        this.mark = mark;

    }

    @Override
    public int getCount() {
        return myList.size();
    }

    @Override
    public Object getItem(int i) {
        return myList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
            view = inflater.inflate(R.layout.template, null);
        }

        TextView page = view.findViewById(R.id.page);
        TextView name = view.findViewById(R.id.name);
        TextView amend = view.findViewById(R.id.amend);
        TextView quantity = view.findViewById(R.id.quantity);
        TextView time = view.findViewById(R.id.time);

        if(mark.equals("spell"))page.setText("咒名：");
        if (mark.equals("maha"))page.setText("經典：");
        if(mark.equals("name"))page.setText("佛號：");

        name.setText(myList.get(i).name);
        amend.setText(myList.get(i).amend);
        quantity.setText(myList.get(i).quantity);
        time.setText(myList.get(i).time);

        return view;
    }


}
