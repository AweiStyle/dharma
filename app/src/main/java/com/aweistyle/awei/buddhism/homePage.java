package com.aweistyle.awei.buddhism;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class homePage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);

        new Handler().postDelayed(new Runnable(){
            public void run() {

                Intent intent = new Intent();
                intent.setClass(homePage.this,manu.class);
                startActivity(intent);

                finish();

            }

        }, 2000);


    }
}
