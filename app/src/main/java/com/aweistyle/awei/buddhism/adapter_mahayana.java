package com.aweistyle.awei.buddhism;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

class Adapter_mahayana extends BaseAdapter {

    String[] mahayanaTag;

    public Adapter_mahayana (String mahayanaTag[]){

        this.mahayanaTag = mahayanaTag;

    }

    @Override
    public int getCount() {
        return mahayanaTag.length;
    }

    @Override
    public Object getItem(int position) {
        return mahayanaTag[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.spinneritem_mahayana,null);

        TextView company = view.findViewById(R.id.getView);

        company.setText(mahayanaTag[position]);

        return view;

    }
}
