package com.aweistyle.awei.buddhism;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class jingangSpell extends AppCompatActivity {

    String dharmaTag[] = {"選單","新增1","新增2","新增3","新增4","新增5","新增6","新增7","新增8","新增9","新增10"};
    Adapter_dharma adapter_dharma;
    String json;
    String dharma_choose;
    int x;
    Spinner dharma_spinner;
    TextView setTitle;
    ArrayList<String> arrayList;
    Gson gson;
    ArrayList<ItemClass> myList;
    String historyJ1;
    ArrayList timestampArr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jingang_spell);

        gson = new Gson();

        Button save = findViewById(R.id.save);
        Button cancel = findViewById(R.id.cancel);
        final Button modify = findViewById(R.id.modify);
        Button addItem = findViewById(R.id.addItem);
        setTitle = findViewById(R.id.setTitle);
        final Button history = findViewById(R.id.history);

        dharma_spinner = findViewById(R.id.dharma_spinner);

        final TextView show_text = findViewById(R.id.show_text);



        json = getSharedPreferences("dharmaTag_edit", MODE_PRIVATE).getString("dharmaTag", "[\"選單\",\"新增1\",\"新增2\",\"新增3\",\"新增4\",\"新增5\",\"新增6\",\"新增7\",\"新增8\",\"新增9\",\"新增10\"]");

        Log.e("loadJson",json);
        dharmaTag = gson.fromJson(json,String[].class);

        adapter_dharma = new Adapter_dharma(dharmaTag);
        final Runnable r = new Runnable() {
            @Override
            public void run() {

                dharma_spinner.setAdapter(adapter_dharma);
                dharma_spinner.setSelection(0);

            }
        };


        runOnUiThread(r);

        dharma_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                arrayList = new ArrayList(Arrays.asList(dharmaTag));
                String temp = arrayList.get(dharma_spinner.getSelectedItemPosition());

                if(position == 0){

                    setTitle.setText("");
                    show_text.setText("");

                }else if (position == 1){

                    x = getSharedPreferences("a1", MODE_PRIVATE).getInt("a", 0);
                    setTitle.setText(temp);
                    show_text.setText("總計：" + x + "遍");
                    Log.e("x",String.valueOf(x));

                }else if (position == 2){

                    x = getSharedPreferences("b1", MODE_PRIVATE).getInt("a", 0);
                    setTitle.setText(temp);
                    show_text.setText("總計：" + x + "遍");
                    Log.e("x",String.valueOf(x));

                }else if (position == 3){

                    x = getSharedPreferences("c1", MODE_PRIVATE).getInt("a", 0);
                    setTitle.setText(temp);
                    show_text.setText("總計：" + x + "遍");
                    Log.e("x",String.valueOf(x));

                }else if (position == 4){

                    x = getSharedPreferences("d1", MODE_PRIVATE).getInt("a", 0);
                    setTitle.setText(temp);
                    show_text.setText("總計：" + x + "遍");
                    Log.e("x",String.valueOf(x));

                }else if (position == 5){

                    x = getSharedPreferences("e1", MODE_PRIVATE).getInt("a", 0);
                    setTitle.setText(temp);
                    show_text.setText("總計：" + x + "遍");
                    Log.e("x",String.valueOf(x));

                }else if (position == 6){

                    x = getSharedPreferences("f1", MODE_PRIVATE).getInt("a", 0);
                    setTitle.setText(temp);
                    show_text.setText("總計：" + x + "遍");
                    Log.e("x",String.valueOf(x));

                }else if (position == 7){

                    x = getSharedPreferences("g1", MODE_PRIVATE).getInt("a", 0);
                    setTitle.setText(temp);
                    show_text.setText("總計：" + x + "遍");
                    Log.e("x",String.valueOf(x));

                }
                else if (position == 8){

                    x = getSharedPreferences("h1", MODE_PRIVATE).getInt("a", 0);
                    setTitle.setText(temp);
                    show_text.setText("總計：" + x + "遍");
                    Log.e("x",String.valueOf(x));

                }else if (position == 9){

                    x = getSharedPreferences("i1", MODE_PRIVATE).getInt("a", 0);
                    setTitle.setText(temp);
                    show_text.setText("總計：" + x + "遍");
                    Log.e("x",String.valueOf(x));

                }else if (position == 10){

                    x = getSharedPreferences("j1", MODE_PRIVATE).getInt("a", 0);
                    setTitle.setText(temp);
                    show_text.setText("總計：" + x + "遍");
                    Log.e("x",String.valueOf(x));

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(jingangSpell.this);
                builder.setTitle("溫馨提醒");
                builder.setMessage("確定儲存次數嗎？");
                builder.setIcon(R.drawable.ic_launcher_background);
                builder.setPositiveButton("是", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        String dharma = null;

                        try {

                            EditText input_ET = findViewById(R.id.input_ET);
                            String s = input_ET.getText().toString(); //輸入的次數
                            if (s.equals(""))throw new Error();
                            int i = Integer.parseInt(s);  //x = 總次數 , i = 次數的整數型態

                            if (i == 0){

                                Toast toast2 = Toast.makeText(jingangSpell.this, "追加數字不得為0", Toast.LENGTH_SHORT);
                                toast2.show();

                            }

                            arrayList = new ArrayList(Arrays.asList(dharmaTag));
                            dharma_choose = arrayList.get(dharma_spinner.getSelectedItemPosition());

                            if(dharma_spinner.getSelectedItemPosition() == 0){

                                show_text.setText("");

                                Toast toast2 = Toast.makeText(jingangSpell.this, "請選擇欲修改的持咒", Toast.LENGTH_SHORT);
                                toast2.show();

                                return;

                            }

                            if (dharma_choose.equals("選單")) throw new Error();
                            if (dharma_spinner.getSelectedItemPosition() == 1) dharma = "a1";
                            if (dharma_spinner.getSelectedItemPosition() == 2) dharma = "b1";
                            if (dharma_spinner.getSelectedItemPosition() == 3) dharma = "c1";
                            if (dharma_spinner.getSelectedItemPosition() == 4) dharma = "d1";
                            if (dharma_spinner.getSelectedItemPosition() == 5) dharma = "e1";
                            if (dharma_spinner.getSelectedItemPosition() == 6) dharma = "f1";
                            if (dharma_spinner.getSelectedItemPosition() == 7) dharma = "g1";
                            if (dharma_spinner.getSelectedItemPosition() == 8) dharma = "h1";
                            if (dharma_spinner.getSelectedItemPosition() == 9) dharma = "i1";
                            if (dharma_spinner.getSelectedItemPosition() == 10) dharma = "j1";

                            int y = getSharedPreferences(dharma , MODE_PRIVATE).getInt("a",0);

                            x = y + i;

                            show_text.setText("總計：" + x + "遍");
                            input_ET.setText("");

                            Log.e("dharma",dharma);
                            Log.e("x_save",String.valueOf(x));

                            SharedPreferences pref = getSharedPreferences(dharma, MODE_PRIVATE);
                            pref.edit()
                                    .putInt("a", x)
                                    .commit();

                            SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd_hh:mm");
                            format.setTimeZone(TimeZone.getDefault());

                            Date curDate = new Date(System.currentTimeMillis());

                            String str = format.format(curDate);

                            Gson gson = new Gson();


                            String historyJ = getSharedPreferences("historyJingangSpell", MODE_PRIVATE)
                                    .getString("historyArray", "[]");

                            String timeStampStr = getSharedPreferences("timestamp", MODE_PRIVATE)
                                    .getString("timestamp", "[]");

                            Log.e("historyJ",historyJ);
                            Log.e("timestamp",timeStampStr);

                            String str_i = String.valueOf(i);

                            timestampArr = gson.fromJson(timeStampStr,ArrayList.class);

                            myList = gson.fromJson(historyJ,ArrayList.class);

                            myList.add(new ItemClass(dharma_spinner.getSelectedItem().toString(),"追加"+ "(" + str_i + ")" ,String.valueOf(x),str)); //( 名稱 / 內容 / 次數 / 時間 )

                            long timeStamp = System.currentTimeMillis();
                            timestampArr.add(String.valueOf(timeStamp));

                            timeStampStr = gson.toJson(timestampArr);

                            historyJ1 = gson.toJson(myList);

                            SharedPreferences pref1 = getSharedPreferences("historyJingangSpell", MODE_PRIVATE);
                            pref1.edit().putString("historyArray",historyJ1).commit();

                            SharedPreferences pref2 = getSharedPreferences("timestamp", MODE_PRIVATE);
                            pref2.edit().putString("timestamp",timeStampStr).commit();

                        }catch (Exception e){

                            Toast toast2 = Toast.makeText(jingangSpell.this, "請輸入合法數字", Toast.LENGTH_SHORT);
                            toast2.show();

                        }catch (Error error){

                            Toast toast2 = Toast.makeText(jingangSpell.this, "追加次數不得為空", Toast.LENGTH_SHORT);
                            toast2.show();

                        }

                    }

                });

                builder.setNegativeButton("否", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                    }
                });

                builder.show();

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });

        modify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Gson gson = new Gson();

                final EditText edit1 = new EditText(jingangSpell.this);

                AlertDialog.Builder builder = new AlertDialog.Builder(jingangSpell.this);
                builder.setTitle("請輸入要修改的數字");    //设置对话框标题
                builder.setIcon(android.R.drawable.btn_star);

                edit1.setSingleLine();
                edit1.setImeOptions(EditorInfo.IME_ACTION_DONE);

                builder.setView(edit1);
                builder.setPositiveButton("確認", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd_hh:mm");
                        format.setTimeZone(TimeZone.getDefault());

                        Date curDate = new Date(System.currentTimeMillis());

                        String str = format.format(curDate);


                        try {

                            String dharma = null;
                            String modify = edit1.getText().toString();

                            if (modify.equals(""))throw new Error();


                            Integer.parseInt(modify);
                            if (Integer.parseInt(modify)<0){

                                Toast toast2 = Toast.makeText(jingangSpell.this, "不得輸入負數", Toast.LENGTH_SHORT);
                                toast2.show();

                                return;

                            }

                            show_text.setText("總計：" + Integer.parseInt(modify) + "遍");

                            ArrayList<String> arrayList = new ArrayList(Arrays.asList(dharmaTag));
                            dharma_choose = arrayList.get(dharma_spinner.getSelectedItemPosition());

                            if (dharma_spinner.getSelectedItemPosition() == 0){

                                show_text.setText("");

                                Toast toast2 = Toast.makeText(jingangSpell.this, "請選擇欲修改的咒", Toast.LENGTH_SHORT);
                                toast2.show();

                                return;

                            };

                            if (dharma_spinner.getSelectedItemPosition() == 1) dharma = "a1";
                            if (dharma_spinner.getSelectedItemPosition() == 2) dharma = "b1";
                            if (dharma_spinner.getSelectedItemPosition() == 3) dharma = "c1";
                            if (dharma_spinner.getSelectedItemPosition() == 4) dharma = "d1";
                            if (dharma_spinner.getSelectedItemPosition() == 5) dharma = "e1";
                            if (dharma_spinner.getSelectedItemPosition() == 6) dharma = "f1";
                            if (dharma_spinner.getSelectedItemPosition() == 7) dharma = "g1";
                            if (dharma_spinner.getSelectedItemPosition() == 8) dharma = "h1";
                            if (dharma_spinner.getSelectedItemPosition() == 9) dharma = "i1";
                            if (dharma_spinner.getSelectedItemPosition() == 10) dharma = "j1";

                            int temp = getSharedPreferences(dharma , MODE_PRIVATE).getInt("a",0); //原來的次數

                            SharedPreferences pref = getSharedPreferences(dharma, MODE_PRIVATE);
                            pref.edit()
                                    .putInt("a", Integer.valueOf(modify))
                                    .commit();

                            temp = Integer.valueOf(modify) - temp;
                            String m;  //判斷正負號
                            if(temp > 0){
                                m = "+";
                            }else {
                                m = "";
                            }


                            Gson gson = new Gson();

                            String timeStampStr = getSharedPreferences("timestamp", MODE_PRIVATE)
                                    .getString("timestamp", "[]");


                            String historyJ = getSharedPreferences("historyJingangSpell", MODE_PRIVATE)
                                    .getString("historyArray", "[]");

                            Log.e("historyJ",historyJ);
                            Log.e("timestamp",timeStampStr);

                            timestampArr = gson.fromJson(timeStampStr,ArrayList.class);

                            myList = gson.fromJson(historyJ,ArrayList.class);

                            myList.add(new ItemClass(dharma_spinner.getSelectedItem().toString(),"修改"+ "(" + m + temp + ")",modify,str)); //( 名稱 / 內容 / 次數 / 時間 )
                            long timeStamp = System.currentTimeMillis();
                            timestampArr.add(String.valueOf(timeStamp));

                            timeStampStr = gson.toJson(timestampArr);

                            historyJ1 = gson.toJson(myList);

                            SharedPreferences pref1 = getSharedPreferences("historyJingangSpell", MODE_PRIVATE);
                            pref1.edit().putString("historyArray",historyJ1).commit();

                            SharedPreferences pref2 = getSharedPreferences("timestamp", MODE_PRIVATE);
                            pref2.edit().putString("timestamp",timeStampStr).commit();

                            Toast toast = Toast.makeText(jingangSpell.this, "修改內容已存檔", Toast.LENGTH_SHORT);
                            toast.show();


                        } catch (Exception e) {

                            Toast toast2 = Toast.makeText(jingangSpell.this, "請輸入合法數字", Toast.LENGTH_SHORT);
                            toast2.show();

                        }catch (Error error){

                            Toast toast2 = Toast.makeText(jingangSpell.this, "修改次數不得為空", Toast.LENGTH_SHORT);
                            toast2.show();

                        }


                    }
                });
                builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }


                });

                builder.show();

            }
        });

        history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(jingangSpell.this , SpellSearch.class);
                intent.putExtra("mark","spell");
                startActivity(intent);


            }
        });

        addItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                final EditText edit = new EditText(jingangSpell.this);

                AlertDialog.Builder builder = new AlertDialog.Builder(jingangSpell.this);
                builder.setTitle("請輸入要修改的名稱");    //设置对话框标题
                builder.setIcon(android.R.drawable.btn_star);

                edit.setSingleLine();
                edit.setImeOptions(EditorInfo.IME_ACTION_DONE);

                builder.setView(edit);
                builder.setPositiveButton("確認", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if(dharma_spinner.getSelectedItemPosition() == 0){

                            Toast toast2 = Toast.makeText(jingangSpell.this, "請選擇欲新增的按鈕", Toast.LENGTH_SHORT);
                            toast2.show();

                            return;

                        }

                        try {

                            String editItem = edit.getText().toString();


                            if (editItem.equals(""))throw new Error();

                            if (dharma_spinner.getSelectedItemPosition() == 0){

                                Toast toast = Toast.makeText(jingangSpell.this, "請選擇修改項目", Toast.LENGTH_SHORT);
                                toast.show();

                                return;

                            }

                            int position = dharma_spinner.getSelectedItemPosition();
                            dharmaTag[position] = editItem;

                            String json = gson.toJson(dharmaTag);

                            Log.e("dharmaTag",json);

                            SharedPreferences pref = getSharedPreferences("dharmaTag_edit", MODE_PRIVATE);
                            pref.edit()
                                    .putString("dharmaTag", json)
                                    .commit();

                            Toast toast = Toast.makeText(jingangSpell.this, "修改內容已存檔", Toast.LENGTH_SHORT);
                            toast.show();

                            runOnUiThread(r);



                        } catch (Exception e) {


                            Log.e(getClass().getName(), Log.getStackTraceString(e));

                            Log.e("click_catch","click_catch");


                        }catch (Error error){

                            Toast toast2 = Toast.makeText(jingangSpell.this, "修改項目不得為空", Toast.LENGTH_SHORT);
                            toast2.show();

                        }


                    }
                });
                builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }


                });

                builder.show();

            }
        });




    }
}
