package com.aweistyle.awei.buddhism;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;

import com.google.gson.Gson;

public class ExportSelectPage extends AppCompatActivity {
    @Override
    protected void onResume() {
        super.onResume();

        String switch1 = getSharedPreferences("switch", MODE_PRIVATE)
                .getString("switch", "on");

        Log.e("switch1",switch1);

        if (switch1.equals("off")){

            this.finish();

        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_export_select_page);

        final ImageButton btn_spell = findViewById(R.id.btn_spell);
        final ImageButton btn_maha = findViewById(R.id.btn_maha);
        final ImageButton btn_name = findViewById(R.id.btn_name);

        Intent intent = getIntent();
        final String num = intent.getStringExtra("num");
        final String cal = intent.getStringExtra("cal");

        btn_spell.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){

                    btn_spell.setBackgroundResource(R.drawable.budbackground11);


                }else if(motionEvent.getAction() == MotionEvent.ACTION_UP){

                    btn_spell.setBackgroundResource(R.drawable.budbackground1);
                    Intent intent = new Intent(ExportSelectPage.this,ExportSelectPage2.class);
                    intent.putExtra("mark","spell");
                    intent.putExtra("num",num);
                    intent.putExtra("cal",cal);
                    startActivity(intent);

                }

                return false;
            }
        });

        btn_maha.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){

                    btn_maha.setBackgroundResource(R.drawable.budbackground2);


                }else if(motionEvent.getAction() == MotionEvent.ACTION_UP){

                    btn_maha.setBackgroundResource(R.drawable.budbackground22);
                    Intent intent = new Intent(ExportSelectPage.this,ExportSelectPage2.class);
                    intent.putExtra("mark","maha");
                    intent.putExtra("num",num);
                    intent.putExtra("cal",cal);
                    startActivity(intent);


                }

                return false;
            }
        });

        btn_name.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){

                    btn_name.setBackgroundResource(R.drawable.budbackground33);


                }else if(motionEvent.getAction() == MotionEvent.ACTION_UP){

                    btn_name.setBackgroundResource(R.drawable.budbackground3);
                    Intent intent = new Intent(ExportSelectPage.this,ExportSelectPage2.class);
                    intent.putExtra("mark","name");
                    intent.putExtra("num",num);
                    intent.putExtra("cal",cal);
                    startActivity(intent);

                }

                return false;
            }
        });



    }
}
