package com.aweistyle.awei.buddhism;

import android.content.Intent;
import android.support.v4.view.ViewPropertyAnimatorListenerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

public class SpellSearch extends AppCompatActivity {

    String historyJ;
    String timeStampStr;
    ArrayList timestampArr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spell_search);

        Intent intent = getIntent();
        String mark = intent.getStringExtra("mark");

        Gson gson = new Gson();

        ListView show1 = findViewById(R.id.show1);

        if (mark.equals("spell")) {

            historyJ = getSharedPreferences("historyJingangSpell", MODE_PRIVATE)
                    .getString("historyArray", "[]");

            timeStampStr = getSharedPreferences("timestamp", MODE_PRIVATE)
                    .getString("timestamp", "[]");

            Log.e("timestamp",timeStampStr);

        }else if (mark.equals("maha")){

            historyJ = getSharedPreferences("historyJingangSpell", MODE_PRIVATE)
                    .getString("historyArray2", "[]");

            timeStampStr = getSharedPreferences("timestamp", MODE_PRIVATE)
                    .getString("timestamp2", "[]");

            Log.e("timestamp",timeStampStr);

        }else {

            historyJ = getSharedPreferences("historyJingangSpell", MODE_PRIVATE)
                    .getString("historyArray3", "[]");

            timeStampStr = getSharedPreferences("timestamp", MODE_PRIVATE)
                    .getString("timestamp3", "[]");

            Log.e("timestamp",timeStampStr);

        }

        timestampArr = gson.fromJson(timeStampStr,ArrayList.class);

        ArrayList<ItemClass> myList = gson.fromJson(historyJ,new TypeToken<ArrayList<ItemClass>>(){}.getType());

        if (timestampArr.size() == 0)return;

        long x = 604800000; //七天的秒數
        long timeStampToday = System.currentTimeMillis();  //當前系統的時間戳記

        for (int i = 0; i < timestampArr.size() ; i++) {

            long temp = Long.parseLong(timestampArr.get(i).toString());
            if (timeStampToday - temp > x) {

                timestampArr.remove(i);
                myList.remove(i);

            }



        ViewAdapterSearch adapter = new ViewAdapterSearch(myList);
        adapter.mark = mark;
        show1.setAdapter(adapter);


    }
}
}
