package com.aweistyle.awei.buddhism;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Looper;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

public class calculation extends AppCompatActivity {

    String json;
    Gson gson;
    int x;
    String[] item = {"選單","新增1","新增2","新增3","新增4","新增5","新增6","新增7","新增8","新增9","新增10"};
    Spinner calculation_spinner;
    Adapter_mahayana adapter_mahayana;
    ArrayList<String> arrayList;
    String item_choose;
    String cal_choose;
    String cal = null;

    @Override
    protected void onResume() {
        super.onResume();

        calculation_spinner.setSelection(0);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculation);

        final TextView show = findViewById(R.id.show);
        final ImageButton add = findViewById(R.id.add);
        Button addItem = findViewById(R.id.addItem);
        calculation_spinner = findViewById(R.id.calculation_spinner);
        final TextView showBook = findViewById(R.id.showBook);
        gson = new Gson();
        Button modify = findViewById(R.id.modify);
        Button export = findViewById(R.id.export);

        json = getSharedPreferences("item", MODE_PRIVATE).getString("item", "[\"選單\",\"新增1\",\"新增2\",\"新增3\",\"新增4\",\"新增5\",\"新增6\",\"新增7\",\"新增8\",\"新增9\",\"新增10\"]");
        item = gson.fromJson(json,String[].class);

        Log.e("json",json);

        adapter_mahayana = new Adapter_mahayana(item);

        final Runnable r = new Runnable() {
            @Override
            public void run() {

                calculation_spinner.setAdapter(adapter_mahayana);
                calculation_spinner.setSelection(0);

            }
        };


        runOnUiThread(r);

        calculation_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                arrayList = new ArrayList(Arrays.asList(item));
                String temp = arrayList.get(calculation_spinner.getSelectedItemPosition());

                if(position == 0){

                    showBook.setText("");
                    show.setText("");

                }else if (position == 1){

                    x = getSharedPreferences("a", MODE_PRIVATE).getInt("x", 0);
                    showBook.setText(temp);
                    show.setText("總計：" + x + "遍");

                }else if (position == 2) {

                    x = getSharedPreferences("b", MODE_PRIVATE).getInt("x", 0);
                    showBook.setText(temp);
                    show.setText("總計：" + x + "遍");

                }else if (position == 3) {

                    x = getSharedPreferences("c", MODE_PRIVATE).getInt("x", 0);
                    showBook.setText(temp);
                    show.setText("總計：" + x + "遍");
                }else if (position == 4){

                    x = getSharedPreferences("d", MODE_PRIVATE).getInt("x", 0);
                    showBook.setText(temp);
                    show.setText("總計：" + x + "遍");

                }else if (position == 5) {

                    x = getSharedPreferences("e", MODE_PRIVATE).getInt("x", 0);
                    showBook.setText(temp);
                    show.setText("總計：" + x + "遍");

                }else if (position == 6) {

                    x = getSharedPreferences("f", MODE_PRIVATE).getInt("x", 0);
                    showBook.setText(temp);
                    show.setText("總計：" + x + "遍");

                }else if (position == 7) {

                    x = getSharedPreferences("g", MODE_PRIVATE).getInt("x", 0);
                    showBook.setText(temp);
                    show.setText("總計：" + x + "遍");
                }else if (position == 8) {

                    x = getSharedPreferences("h", MODE_PRIVATE).getInt("x", 0);
                    showBook.setText(temp);
                    show.setText("總計：" + x + "遍");
                }else if (position == 9) {

                    x = getSharedPreferences("i", MODE_PRIVATE).getInt("x", 0);
                    showBook.setText(temp);
                    show.setText("總計：" + x + "遍");
                }else if (position == 10) {

                    x = getSharedPreferences("j", MODE_PRIVATE).getInt("x", 0);
                    showBook.setText(temp);
                    show.setText("總計：" + x + "遍");
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        add.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN) //按下重新设置背景图片
                {
                    add.setBackgroundResource(R.drawable.flower2);
                }
                else if(motionEvent.getAction() == MotionEvent.ACTION_UP) //松手恢复原来图片
                {
                    add.setBackgroundResource(R.drawable.flower1);

                    if (calculation_spinner.getSelectedItemPosition() == 0) {

                        Toast toast2 = Toast.makeText(calculation.this, "請選擇項目", Toast.LENGTH_SHORT);
                        toast2.show();

                        return false;

                    }

                    arrayList = new ArrayList(Arrays.asList(item));
                    item_choose = arrayList.get(calculation_spinner.getSelectedItemPosition());

                    if (calculation_spinner.getSelectedItemPosition() == 1){cal = "a";}
                    if (calculation_spinner.getSelectedItemPosition() == 2){cal = "b";}
                    if (calculation_spinner.getSelectedItemPosition() == 3){cal = "c";}
                    if (calculation_spinner.getSelectedItemPosition() == 4){cal = "d";}
                    if (calculation_spinner.getSelectedItemPosition() == 5){cal = "e";}
                    if (calculation_spinner.getSelectedItemPosition() == 6){cal = "f";}
                    if (calculation_spinner.getSelectedItemPosition() == 7){cal = "g";}
                    if (calculation_spinner.getSelectedItemPosition() == 8){cal = "h";}
                    if (calculation_spinner.getSelectedItemPosition() == 9){cal = "i";}
                    if (calculation_spinner.getSelectedItemPosition() == 10){cal = "j";}

                    x = getSharedPreferences(cal , MODE_PRIVATE).getInt("x",0);

                    x = x + 1;
                    show.setText("總計：" + String.valueOf(x) + "遍");


                    SharedPreferences pref = getSharedPreferences(cal, MODE_PRIVATE);
                    pref.edit()
                            .putInt("x", x)
                            .commit();

                }

                return false;
            }


        });

        addItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final EditText edit = new EditText(calculation.this);

                AlertDialog.Builder builder = new AlertDialog.Builder(calculation.this);
                builder.setTitle("請輸入要修改的名稱");    //设置对话框标题
                builder.setIcon(android.R.drawable.btn_star);

                edit.setSingleLine();
                edit.setImeOptions(EditorInfo.IME_ACTION_DONE);

                builder.setView(edit);
                builder.setPositiveButton("確認", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if(calculation_spinner.getSelectedItemPosition() == 0){

                            Toast toast2 = Toast.makeText(calculation.this, "請選擇欲修改的計數器名稱", Toast.LENGTH_SHORT);
                            toast2.show();

                            return;

                        }

                        try {


                            String modify = edit.getText().toString();


                            if (modify.equals(""))throw new Error();

                            int position = calculation_spinner.getSelectedItemPosition();
                            item[position] = modify;

                            String json = gson.toJson(item);

                            SharedPreferences pref = getSharedPreferences("item", MODE_PRIVATE);
                            pref.edit()
                                    .putString("item", json)
                                    .commit();

                            Toast toast = Toast.makeText(calculation.this, "修改內容已存檔", Toast.LENGTH_SHORT);
                            toast.show();

                            runOnUiThread(r);



                        } catch (Exception e) {


                            Log.e(getClass().getName(), Log.getStackTraceString(e));

                            Log.e("click_catch","click_catch");


                        }catch (Error error){

                            Toast toast2 = Toast.makeText(calculation.this, "修改項目不得為空", Toast.LENGTH_SHORT);
                            toast2.show();

                        }


                    }
                });
                builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }


                });

                builder.show();

            }
        });

        export.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (show.getText().equals("總計：0遍")){

                    Toast toast = Toast.makeText(calculation.this, "遍數為0不得匯出", Toast.LENGTH_SHORT);
                    toast.show();

                    return;

                }

                if (calculation_spinner.getSelectedItemPosition() == 0){

                    Toast toast = Toast.makeText(calculation.this, "請選擇匯出項目", Toast.LENGTH_SHORT);
                    toast.show();

                    return;

                }



                if (calculation_spinner.getSelectedItemPosition() == 1){cal = "a";}
                if (calculation_spinner.getSelectedItemPosition() == 2){cal = "b";}
                if (calculation_spinner.getSelectedItemPosition() == 3){cal = "c";}
                if (calculation_spinner.getSelectedItemPosition() == 4){cal = "d";}
                if (calculation_spinner.getSelectedItemPosition() == 5){cal = "e";}
                if (calculation_spinner.getSelectedItemPosition() == 6){cal = "f";}
                if (calculation_spinner.getSelectedItemPosition() == 7){cal = "g";}
                if (calculation_spinner.getSelectedItemPosition() == 8){cal = "h";}
                if (calculation_spinner.getSelectedItemPosition() == 9){cal = "i";}
                if (calculation_spinner.getSelectedItemPosition() == 10){cal = "j";}

                x = getSharedPreferences(cal , MODE_PRIVATE).getInt("x",0);

                Log.e("cal",cal);
                Log.e("x",String.valueOf(x));

                SharedPreferences pref = getSharedPreferences("switch", MODE_PRIVATE);
                pref.edit()
                        .putString("switch", "on")
                        .commit();

                Intent intent = new Intent(calculation.this,ExportSelectPage.class);
                intent.putExtra("num",String.valueOf(x));
                intent.putExtra("cal",cal);
                startActivity(intent);

            }
        });

        modify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final EditText edit1 = new EditText(calculation.this);

                AlertDialog.Builder builder = new AlertDialog.Builder(calculation.this);
                builder.setTitle("請輸入要修改的數字");    //设置对话框标题
                builder.setIcon(android.R.drawable.btn_star);

                edit1.setSingleLine();
                edit1.setImeOptions(EditorInfo.IME_ACTION_DONE);

                builder.setView(edit1);
                builder.setPositiveButton("確認", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                        try {

                            String modify = edit1.getText().toString();

                            if (modify.equals(""))throw new Error();


                            Integer.parseInt(modify);
                            if (Integer.parseInt(modify)<0){

                                Toast toast2 = Toast.makeText(calculation.this, "不得輸入負數", Toast.LENGTH_SHORT);
                                toast2.show();

                                return;

                            }



                            show.setText("總計：" + Integer.parseInt(modify) + "遍");

                            ArrayList<String> arrayList = new ArrayList(Arrays.asList(item));
                            cal_choose = arrayList.get(calculation_spinner.getSelectedItemPosition());

                            if (calculation_spinner.getSelectedItemPosition() == 0){

                                show.setText("");

                                Toast toast2 = Toast.makeText(calculation.this, "請選擇欲修改的項目", Toast.LENGTH_SHORT);
                                toast2.show();

                                return;

                            }

                            if (calculation_spinner.getSelectedItemPosition() == 1){cal = "a";}
                            if (calculation_spinner.getSelectedItemPosition() == 2){cal = "b";}
                            if (calculation_spinner.getSelectedItemPosition() == 3){cal = "c";}
                            if (calculation_spinner.getSelectedItemPosition() == 4){cal = "d";}
                            if (calculation_spinner.getSelectedItemPosition() == 5){cal = "e";}
                            if (calculation_spinner.getSelectedItemPosition() == 6){cal = "f";}
                            if (calculation_spinner.getSelectedItemPosition() == 7){cal = "g";}
                            if (calculation_spinner.getSelectedItemPosition() == 8){cal = "h";}
                            if (calculation_spinner.getSelectedItemPosition() == 9){cal = "i";}
                            if (calculation_spinner.getSelectedItemPosition() == 10){cal = "j";}

                            Log.e("cal",cal);
                            Log.e("modify", modify);


                            SharedPreferences pref = getSharedPreferences(cal, MODE_PRIVATE);
                            pref.edit()
                                    .putInt("x", Integer.valueOf(modify))
                                    .commit();

                            Toast toast = Toast.makeText(calculation.this, "修改內容已存檔", Toast.LENGTH_SHORT);
                            toast.show();


                        } catch (Exception e) {

                            Toast toast2 = Toast.makeText(calculation.this, "請輸入合法數字", Toast.LENGTH_SHORT);
                            toast2.show();

                        }catch (Error error){

                            Toast toast2 = Toast.makeText(calculation.this, "修改次數不得為空", Toast.LENGTH_SHORT);
                            toast2.show();

                        }


                    }
                });
                builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }


                });

                builder.show();

            }
        });


    }
}
