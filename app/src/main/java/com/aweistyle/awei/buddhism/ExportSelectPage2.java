package com.aweistyle.awei.buddhism;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExportSelectPage2 extends AppCompatActivity {

    String json;
    String[] dharmaTag;
    ArrayList<ItemClass> myList;
    String historyJ1;
    String historyJ2;
    String historyJ3;
    ArrayList timestampArr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_export_select_page2);

        final ListView show = findViewById(R.id.show);

        Intent intent = getIntent();
        final String mark = intent.getStringExtra("mark");
        final String num = intent.getStringExtra("num");
        final String cal = intent.getStringExtra("cal");
        Log.e("num",num);

        Gson gson = new Gson();

        if (mark.equals("spell")) {

            json = getSharedPreferences("dharmaTag_edit", MODE_PRIVATE).getString("dharmaTag", "[\"選單\",\"新增1\",\"新增2\",\"新增3\",\"新增4\",\"新增5\",\"新增6\",\"新增7\",\"新增8\",\"新增9\",\"新增10\"]");

        }else if (mark.equals("maha")) {

            json = getSharedPreferences("mahayanaTag_edit", MODE_PRIVATE).getString("mahayanaTag", "[\"選單\",\"新增1\",\"新增2\",\"新增3\",\"新增4\",\"新增5\",\"新增6\",\"新增7\",\"新增8\",\"新增9\",\"新增10\"]");

        }else {

            json = getSharedPreferences("name_edit", MODE_PRIVATE).getString("nameTag", "[\"選單\",\"新增1\",\"新增2\",\"新增3\",\"新增4\",\"新增5\",\"新增6\",\"新增7\",\"新增8\",\"新增9\",\"新增10\"]");

        }

        Log.e("json",json);

        dharmaTag = gson.fromJson(json, String[].class);

        Adapter_dharma adapter_dharma = new Adapter_dharma(dharmaTag);
        show.setAdapter(adapter_dharma);

        show.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int position, long l) {

                String s = show.getItemAtPosition(position).toString();

                if (show.getItemAtPosition(position).equals("選單"))return;


                final AlertDialog.Builder builder = new AlertDialog.Builder(ExportSelectPage2.this);
                builder.setTitle("匯入資料");
                builder.setMessage("確定要匯入" + "【" + s + "】" + "嗎？");
                builder.setIcon(R.drawable.ic_launcher_background);
                builder.setPositiveButton("是", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        String dharma = num;

                        if(mark.equals("spell")){

                            if (position == 1) dharma = "a1";
                            if (position == 2) dharma = "b1";
                            if (position == 3) dharma = "c1";
                            if (position == 4) dharma = "d1";
                            if (position == 5) dharma = "e1";
                            if (position == 6) dharma = "f1";
                            if (position == 7) dharma = "g1";
                            if (position == 8) dharma = "h1";
                            if (position == 9) dharma = "i1";
                            if (position == 10) dharma = "j1";

                            int y = getSharedPreferences(dharma , MODE_PRIVATE).getInt("a",0);
                            int x = y + Integer.parseInt(num) ;

                            Log.e("dharma",dharma);

                            Log.e("y",String.valueOf(y));
                            Log.e("num",num);
                            Log.e("x",String.valueOf(x));

                            SharedPreferences pref = getSharedPreferences(dharma, MODE_PRIVATE);
                            pref.edit()
                                    .putInt("a", x)
                                    .commit();

                            SharedPreferences pref1 = getSharedPreferences(cal, MODE_PRIVATE);
                            pref1.edit()
                                    .putInt("x", 0)
                                    .commit();




                            SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd_hh:mm");
                            format.setTimeZone(TimeZone.getDefault());

                            Date curDate = new Date(System.currentTimeMillis());

                            String str = format.format(curDate);

                            Gson gson = new Gson();


                            String historyJ = getSharedPreferences("historyJingangSpell", MODE_PRIVATE)
                                    .getString("historyArray", "[]");

                            String timeStampStr = getSharedPreferences("timestamp", MODE_PRIVATE)
                                    .getString("timestamp", "[]");

                            Log.e("historyJ",historyJ);
                            Log.e("timestamp",timeStampStr);

                            timestampArr = gson.fromJson(timeStampStr, ArrayList.class);

                            myList = gson.fromJson(historyJ,ArrayList.class);

                            myList.add(new ItemClass(show.getItemAtPosition(position).toString(),"匯入"+ "(" + num + ")",String.valueOf(x),str)); //( 名稱 / 內容 / 次數 / 時間 )

                            long timeStamp = System.currentTimeMillis();
                            timestampArr.add(String.valueOf(timeStamp));

                            timeStampStr = gson.toJson(timestampArr);

                            historyJ1 = gson.toJson(myList);

                            SharedPreferences pref3 = getSharedPreferences("historyJingangSpell", MODE_PRIVATE);
                            pref3.edit().putString("historyArray",historyJ1).commit();

                            SharedPreferences pref2 = getSharedPreferences("timestamp", MODE_PRIVATE);
                            pref2.edit().putString("timestamp",timeStampStr).commit();


                        }else if (mark.equals("maha")){

                            dharma = num;

                            if (position == 1) dharma = "a";
                            if (position == 2) dharma = "b";
                            if (position == 3) dharma = "c";
                            if (position == 4) dharma = "d";
                            if (position == 5) dharma = "e";
                            if (position == 6) dharma = "f";
                            if (position == 7) dharma = "g";
                            if (position == 8) dharma = "h";
                            if (position == 9) dharma = "i";
                            if (position == 10) dharma = "j";

                            int y = getSharedPreferences(dharma , MODE_PRIVATE).getInt("a",0);
                            int x = y + Integer.parseInt(num) ;


                            SharedPreferences pref = getSharedPreferences(dharma, MODE_PRIVATE);
                            pref.edit()
                                    .putInt("a", x)
                                    .commit();

                            SharedPreferences pref1 = getSharedPreferences(cal, MODE_PRIVATE);
                            pref1.edit()
                                    .putInt("x", 0)
                                    .commit();

                            SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd_hh:mm");
                            format.setTimeZone(TimeZone.getDefault());

                            Date curDate = new Date(System.currentTimeMillis());

                            String str = format.format(curDate);

                            Gson gson = new Gson();

                            String timeStampStr = getSharedPreferences("timestamp", MODE_PRIVATE)
                                    .getString("timestamp2", "[]");

                            String historyJ = getSharedPreferences("historyJingangSpell", MODE_PRIVATE)
                                    .getString("historyArray2", "[]");

                            Log.e("historyJ",historyJ);
                            Log.e("timestamp",timeStampStr);

                            timestampArr = gson.fromJson(timeStampStr,ArrayList.class);

                            myList = gson.fromJson(historyJ,ArrayList.class);

                            myList.add(new ItemClass((String) show.getItemAtPosition(position),"匯入"+ "(" + num + ")",String.valueOf(x),str)); //( 名稱 / 內容 / 次數 / 時間 )
                            long timeStamp = System.currentTimeMillis();
                            timestampArr.add(String.valueOf(timeStamp));

                            timeStampStr = gson.toJson(timestampArr);

                            historyJ2 = gson.toJson(myList);

                            SharedPreferences pref3 = getSharedPreferences("historyJingangSpell", MODE_PRIVATE);
                            pref3.edit().putString("historyArray2",historyJ2).commit();

                            SharedPreferences pref2 = getSharedPreferences("timestamp", MODE_PRIVATE);
                            pref2.edit().putString("timestamp2",timeStampStr).commit();


                        }else {

                            dharma = num;

                            if (position == 1) dharma = "aa";
                            if (position == 2) dharma = "bb";
                            if (position == 3) dharma = "cc";
                            if (position == 4) dharma = "dd";
                            if (position == 5) dharma = "ee";
                            if (position == 6) dharma = "ff";
                            if (position == 7) dharma = "gg";
                            if (position == 8) dharma = "hh";
                            if (position == 9) dharma = "ii";
                            if (position == 10) dharma = "jj";

                            int y = getSharedPreferences(dharma , MODE_PRIVATE).getInt("a",0);
                            int x = y + Integer.parseInt(num) ;


                            SharedPreferences pref = getSharedPreferences(dharma, MODE_PRIVATE);
                            pref.edit()
                                    .putInt("a", x)
                                    .commit();

                            SharedPreferences pref1 = getSharedPreferences(cal, MODE_PRIVATE);
                            pref1.edit()
                                    .putInt("x", 0)
                                    .commit();

                            SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd_hh:mm");
                            format.setTimeZone(TimeZone.getDefault());

                            Date curDate = new Date(System.currentTimeMillis());

                            String str = format.format(curDate);

                            Gson gson = new Gson();

                            String historyJ = getSharedPreferences("historyJingangSpell", MODE_PRIVATE)
                                    .getString("historyArray3", "[]");

                            String timeStampStr = getSharedPreferences("timestamp", MODE_PRIVATE)
                                    .getString("timestamp3", "[]");

                            Log.e("historyJ",historyJ);
                            Log.e("timestamp",timeStampStr);

                            timestampArr = gson.fromJson(timeStampStr,ArrayList.class);

                            myList = gson.fromJson(historyJ,ArrayList.class);

                            myList.add(new ItemClass((String) show.getItemAtPosition(position),"匯入"+ "(" + num + ")",String.valueOf(x),str)); //( 名稱 / 內容 / 次數 / 時間 )

                            long timeStamp = System.currentTimeMillis();
                            timestampArr.add(String.valueOf(timeStamp));

                            timeStampStr = gson.toJson(timestampArr);

                            historyJ3 = gson.toJson(myList);

                            SharedPreferences pref3 = getSharedPreferences("historyJingangSpell", MODE_PRIVATE);
                            pref3.edit().putString("historyArray3",historyJ3).commit();

                            SharedPreferences pref2 = getSharedPreferences("timestamp", MODE_PRIVATE);
                            pref2.edit().putString("timestamp3",timeStampStr).commit();


                        }

                        SharedPreferences pref = getSharedPreferences("switch", MODE_PRIVATE);
                        pref.edit()
                                .putString("switch", "off")
                                .putString("cal_page","off")
                                .commit();

                        finish();

                    }
                });

                builder.setNegativeButton("否", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.dismiss();

                    }
                });

                builder.show();

            }
        });

    }
}
